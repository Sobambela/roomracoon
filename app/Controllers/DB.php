<?php

namespace App;

use App\DotEnv;

class DB
{

    protected $servername = "localhost";
    protected $username = "yongama";
    protected $password = "Appl3tr33@";
    protected $dbname = "racoon";
    private $conn;

    public function __contruct()
    {

        (new DotEnv(__DIR__ . '/../../.env'))->load();
        $this->servername = getenv('DATABASE_HOST');
        $this->username = getenv('DATABASE_USERNAME');
        $this->password = getenv('DATABASE_PASSWORD');
        $this->dbname = getenv('DATABASE_NAME');
        $this->conn = new \mysqli($this->servername, $this->username, $this->password, $this->dbname);

        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }

    public function getAll()
    {
        $items = $this->conn->query('SELECT * FROM items');
        $data = array();
        while($row = $items->fetch_assoc()) {
            array_push($data, [
                'id' => $row["id"], 
                'name' => $row['name'],
                'checked' => $row['checked'], 
                'description' => $row['description']
            ]);
        }
        return $data;
    }

    public function insert(array $item)
    {
        $checked = $item['checked'] == 'on'? 1:0;
        $stmt = $this->conn->prepare("INSERT INTO items (`name`, `description`, `checked`) VALUES (?, ?, ?)");
        $stmt->bind_param('ssi', $item['name'], $item['description'], $checked);
        $stmt->execute();
        return $this->getAll();
    }

    public function update(array $item)
    {

        $checked = $item['checked'] == 'on'? 1:0;
        $stmt = $this->conn->prepare("UPDATE items SET `name` = ?, `description`= ?, `checked`= ? WHERE `id` = ?"); 
        $stmt->bind_param('ssii', $item['name'], $item['description'],$checked, $item['id']);
        $stmt->execute();
        return $this->getAll();
    }

    public function check(int $id)
    {
        $stmt = $this->conn->prepare("UPDATE items SET `checked` = IF(`checked` = 1, 0, 1) WHERE id = ?");        
        $stmt->bind_param('i', $id);
        $stmt->execute();
        return $this->getAll();
    }

    public function delete(int $id)
    {
        $stmt = $this->conn->prepare("DELETE FROM items WHERE id = ?");        
        $stmt->bind_param('i', $id);
        $stmt->execute();
        return $this->getAll();
    }
}
