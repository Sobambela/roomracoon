<?php

namespace App;

class Controller extends DB{

    public function __contruct()
    {
        parent::__contruct();
    }

    public function all(){
        parent::__contruct();
        return $this->getAll();
    }

    public function insertRecord(array $items){
        parent::__contruct();
        return $this->insert($items);
    }

    public function updateRecord(array $item){
        parent::__contruct();
        return $this->update($item);
    }

    public function checkRecord($id){
        parent::__contruct();
        return $this->check($id);
    }

    public function deleteRecord($id){
        parent::__contruct();
        return $this->delete($id);
    }
}