#Requirements

- PHP > 7.0
- MySql Database

#Instructions

- Update the file /.env with the necessary database information.
- Create the table in your databse as illustrated below:
```
CREATE TABLE `items` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `checked` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

