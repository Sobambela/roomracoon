<?php
include 'autoload.php';

use App\Controller;

$items = new Controller();

$response = '';
if ($_POST) {

    $action =  $_POST['action'];
    if($action == 'add'){
        $items->insertRecord($_POST);
        $response = "Item Added Successfully";
    }

    if($action == 'edit'){
        $items->updateRecord($_POST);
        $response = "Item Updated Successfully";
    }

    if(isset($_POST['delete'])){
        $items->deleteRecord($_POST['delete']);
        $response = "Item Deleted Successfully";
    }

}
$items = $items->all();
?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>RoomRacoon</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="assets/styles.css" rel="stylesheet">
</head>

<body>

    <header>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <a href="/" class="navbar-brand d-flex align-items-center">
                    <strong>RoomRacoon</strong>
                </a>
            </div>
        </div>
    </header>

    <main role="main">

        <div class="album py-5 bg-light">
            <div class="container">
            <?php if($response !== ''){  ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success" role="alert">
                            <?php echo $response; ?>
                        </div>
                    </div>
                </div>
            <?php }  ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group add-button">
                                        <button type="button" class="btn btn-primary add-item" data-toggle="modal" data-target="#item-modal">Add Item</button>
                                    </div>
                                </div>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">Checked</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(count($items) > 0):
                                            foreach ($items as  $key => $item) {
                                        ?>
                                            <tr>
                                                <th scope="row"><?php echo $key + 1  ?></th>
                                                <td><?php echo $item['name'] ?></td>
                                                <td><?php echo $item['description'] ?></td>
                                                <td scope="row">
                                                    <input class="form-check-input" type="checkbox" <?php echo $item['checked'] ? 'checked' : ''; ?> data-id="<?php echo $item['id'] ?>">
                                                </td>
                                                </td>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" id="edit" data-id="<?php echo $item['id'] ?>" data-name="<?php echo $item['name'] ?>" data-checked="<?php echo $item['checked'] ?>" data-desc="<?php echo $item['description'] ?>" class="btn btn-sm btn-outline-secondary">Edit</button>

                                                        <form action="index.php" method="post">
                                                            <input type="hidden" id="action-<?php echo $item['id'] ?>" name="delete" value="<?php echo $item['id'] ?>">
                                                            <button type="submit" id="delete" class="btn btn-sm btn-outline-secondary">Delete</button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php
                                            }
                                        else:
                                        ?>
                                        <tr>
                                            <th scope="row" colspan="7">0 Items at present, Please add Items</th>
                                        </tr>
                                        <?php
                                        endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="item-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="item-modal-title">Add Item</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="index.php" method="post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Item Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Item Name">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input type="text" class="form-control" name="description" id="description" placeholder="Item description">
                                </div>
                                <div class="form-group">
                                    <label for="col-lg-12">Checked
                                    </label>
                                    <input class="form-check-input" type="checkbox" name="checked" id="checked">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="action" name="action" value="add">
                                <input type="hidden" id="id" name="id" value="">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary modal-submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="assets/script.js"></script>
</body>

</html>