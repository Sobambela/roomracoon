$(document).ready(function(){
    var modalTitle = document.getElementById("item-modal-title");
    modalTitle.innerHTML = 'Add Item';

    $(document).on('click', '#edit', function(){
        var id = $(this).attr('data-id');
        modalTitle.innerHTML = 'Edit Item';
        $('#name').val( $(this).attr('data-name') );
        $('#description').val( $(this).attr('data-desc') );
        if( $(this).attr('data-checked') == 1 ){
            $('#checked').prop('checked', true);
        }
        $('#action').val('edit');
        $('#id').val(id);
        $('button[data-target="#item-modal"]').trigger('click');
    });
});


